
const csv = require('csv-parser');
const fs = require('fs');

const capital = [0, 0, 0, 0, 0];
const principleBusiness = {};
const years = {};
const aggrigate = {};

fs.createReadStream('../csv-file/maharastra.csv')
  .pipe(csv())
  .on('data', (line) => {
    // one
    if (parseInt(line.AUTHORIZED_CAP, 0) < 1e5) {
      capital[0] += 1;
    }

    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1e5
        && parseInt(line.AUTHORIZED_CAP, 0) < 1e6) {
      capital[1] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1e6
        && parseInt(line.AUTHORIZED_CAP, 0) < 1e7) {
      capital[2] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1e7
        && parseInt(line.AUTHORIZED_CAP, 0) < 1e9) {
      capital[3] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1e9) {
      capital[4] += 1;
    }
    // two
    const dateSplit = line.DATE_OF_REGISTRATION.split('-');
    const year = dateSplit[2];
    if (year > 2000 && year <= 2018) {
      if (!years[year]) {
        years[year] = 0;
      }
      years[year] += 1;
    }
    // ..
    const company = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;
    // three
    if (year === '2015') {
      if (!principleBusiness[company]) {
        principleBusiness[company] = 0;
      }
      principleBusiness[company] += 1;
    }
    // four
    let temp;
    if (year > '2010' && year <= '2018') {
      if (!aggrigate[company]) {
        aggrigate[company] = {};
        temp = aggrigate[company];
        if (!temp[year]) {
          temp[year] = 1;
        }
      } else {
        temp = aggrigate[company];
        if (!temp[year]) {
          temp[year] = 1;
        } else {
          temp[year] += 1;
        }
      }
    }
  })
  .on('end', () => {
    // one
    fs.writeFile('../authorised-capital.json', JSON.stringify(capital), () => {});
    // two
    fs.writeFile('../date-of-registration.json', JSON.stringify(years), () => {});
    // three
    fs.writeFile('../business-activity-2015.json', JSON.stringify(principleBusiness), () => {});
    // four
    fs.writeFile('../stacked-bar.json', JSON.stringify(aggrigate), () => {});
  });
