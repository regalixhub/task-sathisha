fetch('./stacked-bar.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = [2010];
    for (let i = 0; year[i] < 2019; i += 1) {
      year.push(1 + year[i]);
    }
    const serarr = [];
    const pba = Object.keys(data);
    for (let i = 0; i < pba.length; i += 1) {
      serarr.push(
        {
          name: pba[i],
          data: Object.values(data[pba[i]]),
        },
      );
    }
    // ...
    Highcharts.chart('plot4', {
      chart: {
        type: 'bar',
      },
      title: {
        text: 'Stacked bar chart of yearwise company registration for particular Business activity',
      },
      xAxis: {
        categories: year,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'companies registered on that year',
        },
      },
      legend: {
        reversed: true,
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: serarr,
    });
  });
