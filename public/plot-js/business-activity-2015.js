fetch('./business-activity-2015.json')
  .then(resp => resp.json())
  .then((data) => {
    const keysSorted = Object.keys(data).sort((a, b) => data[b] - data[a]);
    const name = [];
    const numofreg = [];
    for (let i = 1; i < 11; i += 1) {
      name.push(keysSorted[i]);
      numofreg.push(data[keysSorted[i]]);
    }
    Highcharts.chart('plot3', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Registrations by PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN',
      },
      xAxis: {
        categories: name,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN',
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: 'Companies have PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN: <b>{point.y:.1f}</b>',
      },
      series: [{
        name: 'Population',
        data: numofreg,
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          rotation: -45,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
