fetch('./date-of-registration.json')
  .then(resp => resp.json())
  .then((data) => {
    const key = Object.keys(data);
    const value = Object.values(data);

    Highcharts.chart('plot2', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Year wise company registration',
      },
      xAxis: {
        categories: key,
      },
      yAxis: {
        min: 0,
        title: {
          text: 'registered companies count',
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: 'Number of companies registered in the year: <b>{point.y:.1f}</b>',
      },
      series: [{
        name: 'Population',
        data: value,
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
